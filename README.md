# About

Example of news portal with jwt authorization

# Prerequisites

- nodeJS
- mongoDB

# Installation

1. ### Clone repo

    ```sh
      git clone https://gitlab.com/Lyre_/news-portal-example.git
    ```

   Enter the project root directory

    ```sh
      cd ./news-portal-example
    ```

2. ### API
   Enter the project api directory

    ```sh
    cd ./api
    ```
   Install dependencies
    ```sh
    npm i
    ```
   Create `.env` file
    ```sh
    touch .env
    ```
   Fill `.env` file with data like:
    ```
   CLIENT_URL=http://localhost:3000
   JWT_ACCESS_SECRET=jwt-secret-key
   JWT_REFRESH_SECRET=jwt-refresh-secret-key
   MONGODB_URL=mongodb://localhost/news_test_db
   PORT=8000
    ```
   Generate types for `.env` file 
    ```sh
    npm run gen-env
    ```
   Run API 
    ```sh
    npm run start:dev
    ```

4. ### WEB
   Enter the project web directory
    ```sh
    cd ../web
    ```
   Install dependencies
    ```sh
    npm i
    ```
   Run WEB
    ```sh
    npm start
    ```
