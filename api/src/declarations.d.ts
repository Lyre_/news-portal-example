import { IUserDoc } from './types';

declare module 'xid' {
  const x: any;
  export = x;
}

declare module 'express-serve-static-core' {
  interface Request {
    user?: IUserDoc | null;
  }
}
