import cors from 'cors';
import 'dotenv-safe/config';
import express from 'express';
import cookieParser from 'cookie-parser';
import mongoose from 'mongoose';
import exitHook from 'async-exit-hook';
import user from './routers/user/user.router';
import news from './routers/news/news.router';

const app = express();
const PORT = process.env.PORT || 8000;
app.use(express.json());
app.use(cookieParser());
app.use(
  cors({
    credentials: true,
    origin: process.env.CLIENT_URL,
  })
);
app.use('/uploads', express.static(__dirname + '/public/uploads'));

app.get('/', (_, res) => {
  res.send('Hello world');
});

app.use('/user', user);
app.use('/news', news);

app.use((err) => {
  console.log('Why are you not working ?', err);
  return;
});

const main = async () => {
  await mongoose.connect(process.env.MONGODB_URL as any);

  app.listen(PORT, () => {
    console.log(`Server is started on ${PORT} port !`);
  });

  exitHook(() => {
    console.log('exiting');
    mongoose.disconnect();
  });
};

main().catch((e) => console.error(e));
