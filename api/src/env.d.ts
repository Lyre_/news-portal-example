declare global {
  namespace NodeJS {
    interface ProcessEnv {
      CLIENT_URL: string;
      JWT_ACCESS_SECRET: string;
      JWT_REFRESH_SECRET: string;
      MONGODB_URL: string;
      PORT?: string;
    }
  }
}

export {}
