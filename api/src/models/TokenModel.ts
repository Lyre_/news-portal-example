import { Document, model, Schema } from 'mongoose';

export interface ITokenDoc extends IToken, Document {}

interface IToken {
  user: string;
  refreshToken: string;
}

const TokenSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  refreshToken: { type: String, required: true },
});
const TokenModel = model<ITokenDoc>('Token', TokenSchema);

export default TokenModel;
