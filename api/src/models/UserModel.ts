import { model, Schema } from 'mongoose';
import bcrypt from 'bcrypt';
import { IUserDoc } from '../types';

function validateEmail(email: string): boolean {
  const re = /\S+@\S+\.\S+/;
  return re.test(email);
}

const UserSchema = new Schema({
  email: {
    type: String,
    required: [true, 'Email is required field'],
    unique: true,
    validate: {
      validator: validateEmail,
      message: 'Wrong email pattern',
    },
  },
  password: {
    type: String,
    required: [true, 'Password is required field'],
  },

  name: {
    type: String,
    unique: true,
    required: [true, 'Name is required field'],
  },
  avatar: String,
  created_at: {
    type: Date,
    required: true,
    default: new Date(),
  },
});

UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();

  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);

  next();
});

UserSchema.set('toJSON', {
  transform: (_, ret) => {
    delete ret.password;
    return ret;
  },
});

const UserModel = model<IUserDoc>('User', UserSchema);

export default UserModel;
