import { model, Schema } from 'mongoose';
import { INewsDoc } from '../types';

const NewsSchema = new Schema({
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: [true, 'Author id is required field'],
  },
  title: {
    type: String,
    required: [true, 'Title is required field'],
  },
  image: {
    type: String,
    required: [true, 'Image is required field'],
  },
  content: {
    type: String,
    required: [true, 'Content is required field'],
  },
  created_at: {
    type: Date,
    required: true,
    default: new Date(),
  },
});

const NewsModel = model<INewsDoc>('News', NewsSchema);

export default NewsModel;
