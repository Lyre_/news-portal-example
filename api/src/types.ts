import { Schema, Document } from 'mongoose';

export type UploadFile = Express.Multer.File | undefined;

export interface IUser {
  email: string;
  password: string;
  name: string;
  avatar?: string;
  created_at?: Date;
}

export interface IUserDoc extends IUser, Document {}

export interface IUserFromClient extends IUser {
  confirmPassword: string;
}

export interface INews {
  author: typeof Schema.Types.ObjectId;
  title: string;
  content: string;
  image: string;
  created_at?: Date;
}

export interface INewsDoc extends INews, Document {}

export interface INewsFromClient extends Omit<INews, 'created_at' | 'image' | 'author'> {}
