import NewsModel from '../../models/NewsModel';
import { IUserDoc } from '../../types';

interface INewsInput {
  title: string;
  author: IUserDoc | undefined | null;
  content: string;
  image: Express.Multer.File | undefined;
}

class NewsService {
  fetchNews() {
    return NewsModel.find({}).populate('author');
  }

  fetchOneNews(id: string) {
    return NewsModel.findOne({ _id: id }).populate('author');
  }

  async createNews({ image, author, content, title }: INewsInput) {
    const news = new NewsModel({
      author,
      content,
      title,
      image: image ? '/uploads/' + image.filename : undefined,
    });
    await news.save();
    return news;
  }

  async updateNews({ id, image, author, content, title }: INewsInput & { id: string }) {
    const doc = await NewsModel.findOneAndUpdate(
      { _id: id },
      {
        author,
        content,
        title,
        image: image ? '/uploads/' + image.filename : undefined,
      },
      { returnDocument: 'after' }
    ).populate('author');
    return doc;
  }

  deleteNews(id: string) {
    return NewsModel.findOneAndDelete({ _id: id });
  }
}

export default new NewsService();
