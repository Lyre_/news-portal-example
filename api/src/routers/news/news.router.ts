import express from 'express';
import newsController from './news.controller';
import upload from '../../middlewares/upload';
import authForUser from '../../middlewares/userAuth';

const router = express.Router();

router.get('/', newsController.fetchNews);
router.get('/:id', newsController.fetchOneNews);
router.post('/add', authForUser, upload.single('image'), newsController.createNews);
router.put('/edit', authForUser, upload.single('image'), newsController.updateNews);
router.delete('/delete/:id', authForUser, newsController.deleteNews);

export default router;
