import { Request, Response } from 'express';
import { INewsFromClient } from '../../types';
import errorHandler from '../../middlewares/errorHandler';
import newsService from './news.service';

class NewsController {
  async fetchNews(req: Request<{}, {}, INewsFromClient>, res: Response): Promise<any> {
    try {
      const news = await newsService.fetchNews();
      return res.json(news);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }

  async fetchOneNews(req: Request<{ id: string }>, res: Response): Promise<any> {
    try {
      const news = await newsService.fetchOneNews(req.params.id);
      return res.json(news);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }

  async createNews(req: Request<{}, {}, INewsFromClient>, res: Response): Promise<any> {
    try {
      const { title, content } = req.body;
      const news = await newsService.createNews({ title, author: req.user, content, image: req.file });
      return res.json(news);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }

  async updateNews(req: Request<{}, {}, INewsFromClient & { id: string }>, res: Response): Promise<any> {
    try {
      const { id, title, content } = req.body;
      const news = await newsService.updateNews({
        id,
        title: title,
        author: req.user,
        content: content,
        image: req.file,
      });
      return res.json(news);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }

  async deleteNews(req: Request<{ id: string }>, res: Response): Promise<any> {
    try {
      const { id } = req.params;
      await newsService.deleteNews(id);
      return res.json({ message: 'Ok' });
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
}

export default new NewsController();
