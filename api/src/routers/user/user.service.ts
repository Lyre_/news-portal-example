import UserDto from './dto/user.dto';
import UserModel from '../../models/UserModel';
import { IUser, IUserFromClient, UploadFile } from '../../types';
import ApiError from '../../exceptions/apiError';
import bcrypt from 'bcrypt';
import tokenService from '../../services/tokenService';
import { UserLoginBody } from './user.controller';

interface IRegUserData extends Omit<IUserFromClient, 'created_at' | 'avatar'> {}

class UserService {
  async login({ email, password, name }: UserLoginBody) {
    const user = await UserModel.findOne({ $or: [{ email: email }, { name: name }] });
    if (!user) {
      const message = 'Login or email is incorrect';
      throw ApiError.BadInputRequest(message, {
        email: { message },
        name: { message },
      });
    }

    const isPassEquals = await bcrypt.compare(password, user.password);
    if (!isPassEquals) {
      const message = 'Password  is incorrect';
      throw ApiError.BadInputRequest(message, { password: { message } });
    }

    const userDto = new UserDto(user);
    const tokens = tokenService.generateTokens({ ...userDto });
    await tokenService.saveTokenUser(userDto._id, tokens.refreshToken);

    return { user: userDto, tokens };
  }

  async registration(userData: IRegUserData, imgFile: UploadFile) {
    if (userData.password !== userData.confirmPassword) {
      const message = `Password mismatch`;
      throw ApiError.BadInputRequest(message, { password: { message }, confirmPassword: { message } });
    }
    const input: IUser = {
      ...userData,
      avatar: imgFile ? '/uploads/' + imgFile.filename : undefined,
      created_at: new Date(),
    };
    const user = new UserModel(input);
    const userDto = new UserDto(user);
    const tokens = tokenService.generateTokens({ ...userDto });
    await tokenService.saveTokenUser(userDto._id, tokens.refreshToken);

    await user.save();
    return { user: userDto, tokens };
  }

  async logout(refreshToken: string) {
    await tokenService.removeToken(refreshToken);
  }

  async refresh(refreshToken: string | undefined) {
    if (!refreshToken) throw ApiError.UnauthorizedError();

    const userData = tokenService.validateRefreshToken(refreshToken);
    const tokenFromDb = await tokenService.findToken(refreshToken);

    if (!userData || !tokenFromDb) {
      throw ApiError.UnauthorizedError();
    }

    const user = await UserModel.findById(userData._id);
    if (!user) throw ApiError.UnauthorizedError();
    const userDto = new UserDto(user);
    const tokens = tokenService.generateTokens({ ...userDto });
    await tokenService.saveTokenUser(userDto._id, tokens.refreshToken);

    return { user: userDto, tokens };
  }
}

export default new UserService();
