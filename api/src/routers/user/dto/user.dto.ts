import { IUserDoc } from '../../../types';

export default class UserDto {
  _id;
  email;
  name;
  created_at;
  avatar;

  constructor(model: IUserDoc) {
    this._id = model._id;
    this.email = model.email;
    this.name = model.name;
    this.created_at = model.created_at;
    this.avatar = model.avatar;
  }
}
