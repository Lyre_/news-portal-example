import express from 'express';
import userController from './user.controller';
import upload from '../../middlewares/upload';

const router = express.Router();

router.get('/refresh', userController.refresh);
router.post('/sign_in', userController.login);
router.post('/sign_up', upload.single('avatar'), userController.registration);
router.delete('/logout', userController.logout);

export default router;
