import userService from './user.service';

import { Request, Response } from 'express';
import { IUserFromClient } from '../../types';
import errorHandler from '../../middlewares/errorHandler';

export interface UserLoginBody {
  email?: string;
  name?: string;
  password: string;
}

class UserController {
  async login(req: Request<{}, {}, UserLoginBody>, res: Response): Promise<any> {
    try {
      const { email, name, password } = req.body;
      const { user, tokens } = await userService.login({ email, name, password });
      res.cookie('refreshToken', tokens.refreshToken, { maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true });
      return res.json(user);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }

  async registration(req: Request<{}, {}, IUserFromClient>, res: Response): Promise<any> {
    try {
      const { email, name, password, confirmPassword } = req.body;
      const { user, tokens } = await userService.registration({ email, name, password, confirmPassword }, req.file);
      res.cookie('refreshToken', tokens.refreshToken, { maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true });
      return res.json(user);
    } catch (err) {
      errorHandler(err, req, res);
    }
  }

  async logout(req: Request, res: Response): Promise<any> {
    try {
      const { refreshToken } = req.cookies as { refreshToken: string | undefined };
      if (refreshToken) await userService.logout(refreshToken);
      res.clearCookie('refreshToken');
      return res.json({ message: 'Success', status: true });
    } catch (err) {
      errorHandler(err, req, res);
    }
  }

  async refresh(req: Request, res: Response): Promise<any> {
    try {
      const { refreshToken }: { refreshToken: string | undefined } = req.cookies;
      const { user, tokens } = await userService.refresh(refreshToken);
      res.cookie('refreshToken', tokens.refreshToken, { maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true });
      return res.json({ user, accessToken: tokens.accessToken });
    } catch (err) {
      errorHandler(err, req, res);
    }
  }
}

export default new UserController();

export class UserInputBody {}
