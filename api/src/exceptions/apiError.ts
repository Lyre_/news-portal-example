import { UserInputBody } from '../routers/user/user.controller';

interface InputErr {
  message: string;
}

export type InpErrMap<T> = {
  [Property in keyof T]?: InputErr;
};

export interface IErr<T> {
  message: string;
  errors: InpErrMap<T>;
}

export default class ApiError extends Error {
  status;
  errors;

  constructor(status: number, message: string, errors?: InpErrMap<UserInputBody>) {
    super(message);
    this.status = status;
    this.errors = errors;
  }

  static UnauthorizedError(msg?: string) {
    return new ApiError(401, msg || 'User not authorized');
  }

  static BadInputRequest(message: string, errors: InpErrMap<UserInputBody>) {
    return new ApiError(400, message, errors);
  }
}
