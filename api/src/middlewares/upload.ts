import multer from 'multer';
import { UPLOAD_PATH } from '../constants';
import { v4 as uuid } from 'uuid';
import path from 'path';

const storage = multer.diskStorage({
  destination: (_, __, cb) => {
    cb(null, UPLOAD_PATH);
  },
  filename: (_, file, cb) => {
    cb(null, uuid() + path.extname(file.originalname));
  },
});

const upload = multer({ storage });

export default upload;
