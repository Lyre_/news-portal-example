import { Error } from 'mongoose';
import ApiError, { IErr } from '../exceptions/apiError';
import { Request, Response } from 'express';
import { UserInputBody } from '../routers/user/user.controller';

export default function errorHandler(err: ApiError | Error, _: Request, res: Response) {
  //console.log(`Error: !-!-!- ${err.message} -!-!-!`);

  if (err instanceof ApiError) {
    return res.status(err.status).send({ message: err.message, errors: err.errors });
  }
  if (err.name === 'ValidationError') {
    err.message = 'MongooseValidationError';
    return res.status(400).json(err);
  }
  if (/^(?=.*duplicate)(?=.*email).*$/im.test(err.message)) {
    const message = 'This email is already in use';
    const resErr: IErr<UserInputBody> = { message, errors: { email: { message } } };
    return res.status(400).json(resErr);
  }
  if (/^(?=.*duplicate)(?=.*name).*$/im.test(err.message)) {
    const message = 'This name is already in use';
    const resErr: IErr<UserInputBody> = { message, errors: { name: { message } } };
    return res.status(400).json(resErr);
  }
  return res.status(500).send(err);
}
