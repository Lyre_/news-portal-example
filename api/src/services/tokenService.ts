import jwt from 'jsonwebtoken';
import TokenModel from '../models/TokenModel';
import UserDto from '../routers/user/dto/user.dto';

class TokenService {
  generateTokens(payload: UserDto) {
    const accessToken = jwt.sign(payload, process.env.JWT_ACCESS_SECRET, { expiresIn: '30min' });
    const refreshToken = jwt.sign(payload, process.env.JWT_REFRESH_SECRET, { expiresIn: '30d' });
    return {
      accessToken,
      refreshToken,
    };
  }

  async saveTokenUser(userId: string, refreshToken: string) {
    const tokenData = await TokenModel.findOne({ user: userId });
    if (tokenData) {
      tokenData.refreshToken = refreshToken;
      return await tokenData.save();
    }
    return await TokenModel.create({ user: userId, refreshToken });
  }

  removeToken(refreshToken: string) {
    return TokenModel.deleteOne({ refreshToken });
  }

  findToken(refreshToken: string) {
    return TokenModel.findOne({ refreshToken });
  }

  validateAccessToken(token: string) {
    try {
      const data = jwt.verify(token, process.env.JWT_ACCESS_SECRET);
      return data as UserDto | null;
    } catch (err) {
      return null;
    }
  }

  validateRefreshToken(token: string) {
    try {
      const data = jwt.verify(token, process.env.JWT_REFRESH_SECRET);
      return data as UserDto | null;
    } catch (err) {
      return null;
    }
  }
}

export default new TokenService();
