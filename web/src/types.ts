import { store } from './store/store';

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export interface IUser {
  _id: string;
  email: string;
  name: string;
  avatar?: string;
  created_at: Date;
}

export interface IUserRefreshed {
  user: IUser;
  accessToken: string;
}

export interface INews {
  _id: string;
  author: IUser;
  title: string;
  content: string;
  image: string;
  created_at: string;
}

export interface INewsCreateData extends Omit<INews, '_id' | 'created_at' | 'author' | 'image'> {
  image: File | null;
}

export interface InputErr {
  message: string;
}

export type InpErrMap<T> = {
  [Property in keyof T]?: InputErr;
};

export interface IErr<T> {
  message: string;
  errors: InpErrMap<T>;
}
