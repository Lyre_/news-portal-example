export const API_PORT = process.env.PORT || 8000;
export const API_URL = `http://localhost:${API_PORT}`;
