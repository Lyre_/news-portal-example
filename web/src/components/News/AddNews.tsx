import { Stack, TextField } from '@mui/material';
import { ChangeEvent, SyntheticEvent, useState } from 'react';
import Typography from '@mui/material/Typography';
import { newsAdd, newsEdit } from '../../store/news/newsAsync';
import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';
import { INews, INewsCreateData } from '../../types';
import { LoadingButton } from '@mui/lab';

interface Props {
  init?: INews;
  cb?: () => any;
}

const initialState: INewsCreateData = {
  content: '',
  image: null,
  title: '',
};

const AddNews = ({ init, cb }: Props) => {
  const dispatch = useAppDispatch();
  const { client } = useAppSelector((state) => state.users);
  const { createErr, loading } = useAppSelector((state) => state.news);
  const [newsData, setNewsData] = useState(() => {
    if (init) {
      initialState.content = init.content;
      initialState.title = init.title;
    }
    return initialState;
  });

  const inpChanger = (e: ChangeEvent<HTMLInputElement>) => {
    setNewsData((prevState) => ({ ...prevState, [e.target.name]: e.target.value }));
  };

  const inpImgChanger = (e: ChangeEvent<HTMLInputElement>) => {
    setNewsData((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.files ? e.target.files[0] : null,
    }));
  };

  const onSubmit = async (e: SyntheticEvent) => {
    e.preventDefault();
    if (!client) return alert('First you need to login');

    const formData = new FormData();
    Object.keys(newsData).forEach((key) => {
      formData.append(key, (newsData as any)[key]);
    });
    if (init) {
      formData.append('id', init._id);
      await dispatch(newsEdit(formData as any));
    } else {
      await dispatch(newsAdd(formData as any));
    }
    if (cb) cb();
  };

  return (
    <Stack component={'form'} onSubmit={onSubmit} spacing={1} maxWidth={700} mx={'auto'}>
      <Typography variant={'h3'} textAlign={'center'} py={3}>
        {init ? 'Update News' : 'Create news'}
      </Typography>
      <TextField
        error={Boolean(createErr?.title)}
        helperText={createErr?.title?.message || ' '}
        onChange={inpChanger}
        name={'title'}
        variant="outlined"
        label={'title'}
        value={newsData.title}
        required
      />
      <TextField
        error={Boolean(createErr?.content)}
        helperText={createErr?.content?.message || ' '}
        onChange={inpChanger}
        name={'content'}
        multiline
        minRows={4}
        variant="outlined"
        label={'content'}
        value={newsData.content}
        required
      />
      <TextField
        error={Boolean(createErr?.image)}
        helperText={createErr?.image?.message || ' '}
        onChange={inpImgChanger}
        type={'file'}
        name={'image'}
        variant="outlined"
        required
      />
      <LoadingButton loading={loading} type={'submit'} variant={'contained'} color={'success'}>
        Submit
      </LoadingButton>
    </Stack>
  );
};

export default AddNews;
