import { FC } from 'react';
import { INews } from '../../types';
import { CardMedia, Grid, Stack } from '@mui/material';
import { API_URL } from '../../config';
import Typography from '@mui/material/Typography';

interface InfoNewsProps {
  data: INews;
}

const InfoNews: FC<InfoNewsProps> = ({ data }) => {
  return (
    <Stack>
      <Typography variant="h4" component="div">
        {data.title}
      </Typography>
      <Grid
        container
        justifyContent={'space-between'}
        flexWrap={'nowrap'}
        spacing={1}
        alignItems={'center'}
        sx={{
          color: '#737373',
          fontSize: '14px',
        }}
        mb={2}
      >
        <Typography component={Grid} item>
          By {data.author.name}
        </Typography>
        <Typography component={Grid} item sx={{ fontStyle: 'italic', fontSize: '12px' }}>
          {data.created_at}
        </Typography>
      </Grid>
      <CardMedia component="img" height="420" image={API_URL + data.image} alt="green iguana" />
      <Typography variant="body1" maxHeight={120} sx={{ overflow: 'hidden' }} mt={2}>
        {data.content}
      </Typography>
    </Stack>
  );
};

export default InfoNews;
