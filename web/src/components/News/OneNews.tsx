import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';
import { useEffect, useState } from 'react';
import { fetchOneNews, newsDelete } from '../../store/news/newsAsync';
import { useParams } from 'react-router-dom';
import { Button, Grid, Modal, Stack } from '@mui/material';
import InfoNews from './InfoNews';
import Box from '@mui/material/Box';
import AddNews from './AddNews';
import { clearCurrent } from '../../store/news/newsSlice';

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 600,
  maxHeight: '80vh',
  overflowY: 'auto',
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const OneNews = () => {
  const dispatch = useAppDispatch();
  const params = useParams();
  const { current } = useAppSelector((state) => state.news);
  const { client } = useAppSelector((state) => state.users);
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  useEffect(() => {
    if (params.id) dispatch(fetchOneNews(params.id));
    return () => {
      dispatch(clearCurrent());
    };
  }, []);
  return (
    <Stack mt={3} spacing={2}>
      {client?._id !== current?.author._id && <div>You can't change what's not your news</div>}
      <Grid container>
        <Grid flexGrow={1} item mr={2}>
          <Button
            disabled={!client || client._id !== current?.author._id}
            sx={{ width: '100%' }}
            variant={'contained'}
            color={'warning'}
            onClick={handleOpen}
          >
            edit
          </Button>
        </Grid>
        <Grid flexGrow={1} item>
          <Button
            disabled={!client || client._id !== current?.author._id}
            sx={{ width: '100%' }}
            variant={'contained'}
            color={'error'}
            onClick={() => dispatch(newsDelete(current?._id || ''))}
          >
            delete
          </Button>
        </Grid>
      </Grid>
      {current ? <InfoNews data={current} /> : <div>loading...</div>}
      <div>
        <Modal
          open={open}
          onClose={handleClose}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <Box sx={style}>{current && <AddNews init={current} cb={handleClose} />}</Box>
        </Modal>
      </div>
    </Stack>
  );
};

export default OneNews;
