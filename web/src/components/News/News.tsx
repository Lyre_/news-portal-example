import { useEffect } from 'react';
import { fetchNews } from '../../store/news/newsAsync';
import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';
import NewsCard from './NewsCard';
import { Grid } from '@mui/material';

const News = () => {
  const dispatch = useAppDispatch();
  const { data } = useAppSelector((state) => state.news);

  useEffect(() => {
    dispatch(fetchNews());
  }, []);

  return (
    <Grid container justifyContent={'center'} spacing={3} mt={5}>
      {data.length ? (
        data.map((n) => {
          return (
            <Grid item key={n._id}>
              <NewsCard data={n} />
            </Grid>
          );
        })
      ) : (
        <div>loading...</div>
      )}
    </Grid>
  );
};

export default News;
