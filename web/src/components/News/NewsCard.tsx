import { Card, CardActionArea, CardContent, CardMedia, Grid } from '@mui/material';
import Typography from '@mui/material/Typography';
import { INews } from '../../types';
import { API_URL } from '../../config';
import { Link } from 'react-router-dom';
import { NEWS_PATH } from '../../constants';

interface NewsCardProps {
  data: INews;
}

const NewsCard = ({ data }: NewsCardProps) => {
  return (
    <Card sx={{ width: 375 }}>
      <CardActionArea component={Link} to={NEWS_PATH + data._id}>
        <CardMedia component="img" height="120" image={API_URL + data.image} alt="green iguana" />
        <CardContent>
          <Typography variant="h4" component="div">
            {data.title}
          </Typography>
          <Grid
            container
            justifyContent={'space-between'}
            flexWrap={'nowrap'}
            spacing={1}
            alignItems={'center'}
            sx={{
              color: '#737373',
              fontSize: '14px',
            }}
            mb={2}
          >
            <Typography component={Grid} item>
              By {data.author.name}
            </Typography>
            <Typography component={Grid} item sx={{ fontStyle: 'italic', fontSize: '12px' }}>
              {data.created_at}
            </Typography>
          </Grid>
          <Typography variant="body1" maxHeight={120} sx={{ overflow: 'hidden' }}>
            {data.content}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default NewsCard;
