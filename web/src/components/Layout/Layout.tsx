import { FC, ReactNode, useEffect } from 'react';
import MainAppBar from '../MainAppBar/MainAppBar';
import Box from '@mui/material/Box';
import { Container } from '@mui/material';
import { useAppDispatch } from '../../hooks/reduxHooks';
import { checkAuth } from '../../store/user/userAsync';

interface LayoutProps {
  children: ReactNode;
}

const Layout: FC<LayoutProps> = ({ children }) => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(checkAuth());
  }, []);
  return (
    <>
      <MainAppBar />
      <Container>
        <Box sx={{ minHeight: '90vh' }}>{children}</Box>
      </Container>
    </>
  );
};

export default Layout;
