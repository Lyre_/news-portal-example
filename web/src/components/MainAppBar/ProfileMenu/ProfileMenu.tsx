import IconButton from '@mui/material/IconButton';
import AccountCircle from '@mui/icons-material/AccountCircle';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import * as React from 'react';
import { useAppSelector } from '../../../hooks/reduxHooks';

const ProfileMenu = () => {
  const { client } = useAppSelector((state) => state.users);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <IconButton size="large" onClick={handleMenu} color="inherit">
        <AccountCircle />
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {client ? (
          Object.keys(client).map((key, i) => {
            return (
              <MenuItem key={key + i} onClick={handleClose}>
                {key + ' : ' + (client as any)[key]}
              </MenuItem>
            );
          })
        ) : (
          <MenuItem onClick={handleClose}>You are not logged in</MenuItem>
        )}
      </Menu>
    </div>
  );
};

export default ProfileMenu;
