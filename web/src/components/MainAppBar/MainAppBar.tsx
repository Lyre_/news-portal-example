import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import MainMenu from './MainMenu/MainMenu';
import ProfileMenu from './ProfileMenu/ProfileMenu';
import { Button } from '@mui/material';
import { Link } from 'react-router-dom';

const MainAppBar = () => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <MainMenu />
          <Box sx={{ flexGrow: 1 }}>
            <Button component={Link} to={'/'} sx={{ color: 'white' }}>
              News portal
            </Button>
          </Box>
          <ProfileMenu />
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default MainAppBar;
