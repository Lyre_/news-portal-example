import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import * as React from 'react';
import { ADD_NEWS_PATH, NEWS_PATH, SIGN_IN_PATH, SIGN_UP_PATH } from '../../../constants';
import { Link } from 'react-router-dom';
import { useAppDispatch } from '../../../hooks/reduxHooks';
import { clientLogout } from '../../../store/user/userAsync';

const menuItems = [
  { id: Math.random(), name: 'Sign in', path: SIGN_IN_PATH },
  { id: Math.random(), name: 'Sign up', path: SIGN_UP_PATH },
  { id: Math.random(), name: 'News', path: NEWS_PATH },
  { id: Math.random(), name: 'Create news', path: ADD_NEWS_PATH },
  { id: Math.random(), name: 'Logout', path: '' },
];

const MainMenu = () => {
  const dispatch = useAppDispatch();
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = (name: string) => {
    if (name === 'Logout') {
      dispatch(clientLogout());
    }
    setAnchorEl(null);
  };

  return (
    <>
      <IconButton size="large" edge="start" color="inherit" onClick={handleClick} sx={{ mr: 2 }}>
        <MenuIcon />
      </IconButton>
      <Menu anchorEl={anchorEl} open={open} onClose={handleClose}>
        {menuItems.map((i) => (
          <MenuItem key={i.id} component={Link} to={i.path} onClick={() => handleClose(i.name)}>
            {i.name}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

export default MainMenu;
