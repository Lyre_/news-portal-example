const history: any = {
  navigate: null,
  push: (page: string, ...rest: any) => history.navigate(page, ...rest),
};

export default history;
