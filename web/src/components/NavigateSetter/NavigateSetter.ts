import { useNavigate } from 'react-router-dom';
import history from './History';

const NavigateSetter = () => {
  history.navigate = useNavigate();

  return null;
};

export default NavigateSetter;
