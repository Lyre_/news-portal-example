import { Avatar, Stack, TextField } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import Typography from '@mui/material/Typography';
import LoginIcon from '@mui/icons-material/Login';
import { ChangeEvent, SyntheticEvent, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';
import { clientLogin, UserSignInData } from '../../store/user/userAsync';

const initialState = {
  emailOrLogin: '',
  password: '',
};

const SignIn = () => {
  const dispatch = useAppDispatch();
  const { loading, errors } = useAppSelector((state) => state.users);
  const [userData, setUserData] = useState(initialState);
  const inpChanger = (e: ChangeEvent<HTMLInputElement>) => {
    setUserData((prevState) => ({ ...prevState, [e.target.name]: e.target.value }));
  };

  const onSubmit = (e: SyntheticEvent) => {
    e.preventDefault();
    const { password, emailOrLogin } = userData;
    const data: UserSignInData = {
      password,
    };

    /@/.test(userData.emailOrLogin) ? (data.email = emailOrLogin) : (data.name = emailOrLogin);

    dispatch(clientLogin(data));
  };
  return (
    <Stack
      onSubmit={onSubmit}
      component={'form'}
      sx={{ maxWidth: 700, mx: 'auto' }}
      mt={6}
      spacing={1}
    >
      <Avatar sx={{ bgcolor: 'primary.main', p: 2, mx: 'auto' }}>
        <LoginIcon />
      </Avatar>
      <Typography variant={'h3'} textAlign={'center'} py={3}>
        Sign in
      </Typography>
      <TextField
        onChange={inpChanger}
        name={'emailOrLogin'}
        variant="outlined"
        label={'email or login'}
        error={Boolean(errors?.email) || Boolean(errors?.name)}
        helperText={errors?.email?.message || errors?.name?.message || ' '}
        required
      />
      <TextField
        onChange={inpChanger}
        name={'password'}
        type={'password'}
        variant="outlined"
        label={'password'}
        error={Boolean(errors?.password)}
        helperText={errors?.password?.message || ' '}
        required
      />
      <LoadingButton loading={loading} type={'submit'} variant={'contained'} color={'success'}>
        Submit
      </LoadingButton>
    </Stack>
  );
};

export default SignIn;
