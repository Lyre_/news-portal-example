import { ChangeEvent, SyntheticEvent, useState } from 'react';
import { Avatar, Stack, TextField } from '@mui/material';
import LoginIcon from '@mui/icons-material/Login';
import Typography from '@mui/material/Typography';
import { LoadingButton } from '@mui/lab';
import { clientRegistration, UserSignUpData } from '../../store/user/userAsync';
import { useAppDispatch, useAppSelector } from '../../hooks/reduxHooks';

const initialState: UserSignUpData = {
  email: '',
  password: '',
  confirmPassword: '',
  name: '',
  avatar: '',
};

const SignUp = () => {
  const dispatch = useAppDispatch();
  const { loading, regErr } = useAppSelector((state) => state.users);
  const [userData, setUserData] = useState(initialState);

  const inpChanger = (e: ChangeEvent<HTMLInputElement>) => {
    setUserData((prevState) => ({ ...prevState, [e.target.name]: e.target.value }));
  };

  const onSubmit = (e: SyntheticEvent) => {
    e.preventDefault();
    dispatch(clientRegistration(userData));
  };
  return (
    <Stack
      onSubmit={onSubmit}
      component={'form'}
      sx={{ maxWidth: 700, mx: 'auto' }}
      mt={6}
      spacing={1}
    >
      <Avatar sx={{ bgcolor: 'primary.main', p: 2, mx: 'auto' }}>
        <LoginIcon />
      </Avatar>
      <Typography variant={'h3'} textAlign={'center'} py={3}>
        Sign up
      </Typography>
      <TextField
        error={Boolean(regErr?.email)}
        helperText={regErr?.email?.message || ' '}
        onChange={inpChanger}
        name={'email'}
        variant="outlined"
        label={'email'}
        required
      />
      <TextField
        error={Boolean(regErr?.name)}
        helperText={regErr?.name?.message || ' '}
        onChange={inpChanger}
        name={'name'}
        variant="outlined"
        label={'name'}
        required
      />
      <TextField
        onChange={inpChanger}
        name={'password'}
        type={'password'}
        variant="outlined"
        label={'password'}
        error={Boolean(regErr?.password)}
        helperText={regErr?.password?.message || ' '}
        required
      />
      <TextField
        onChange={inpChanger}
        name={'confirmPassword'}
        type={'password'}
        variant="outlined"
        label={'confirm password'}
        error={Boolean(regErr?.confirmPassword)}
        helperText={regErr?.confirmPassword?.message || ' '}
        required
      />
      {/*<TextField onChange={inpChanger} name={'avatar'} variant="outlined" label={'avatar'} />*/}
      <LoadingButton loading={loading} type={'submit'} variant={'contained'} color={'success'}>
        Submit
      </LoadingButton>
    </Stack>
  );
};

export default SignUp;
