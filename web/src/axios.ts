import { USER_REFRESH_TOKEN_URL } from './constants';
import { API_URL } from './config';
import axios from 'axios';
import { IUserRefreshed } from './types';

const $axios = axios.create({
  withCredentials: true,
  baseURL: API_URL,
});

$axios.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('access_token');
    if (token) {
      config.headers = {
        Authorization: `Bearer ${token}`,
      };
    }
    return config;
  },
  (error) => {
    return Promise.reject({ error });
  }
);

$axios.interceptors.response.use(
  (config) => {
    return config;
  },
  async (error) => {
    const originalRequest = error.config;
    if (error.response.status === 401 && error.config && !error.config._isRetry) {
      originalRequest._isRetry = true;
      try {
        const { data } = await axios.get<IUserRefreshed>(API_URL + USER_REFRESH_TOKEN_URL, {
          withCredentials: true,
        });
        localStorage.setItem('access_token', data.accessToken);
        return $axios.request(originalRequest);
      } catch (err: any) {
        if (err.response.status === 401) {
        } else {
          console.log('Access token error');
        }
      }
    }
    return Promise.reject(error);
  }
);

export default $axios;
