import { createSlice } from '@reduxjs/toolkit';
import { IErr, INews, INewsCreateData, InpErrMap } from '../../types';
import { fetchNews, fetchOneNews, newsAdd, newsDelete, newsEdit } from './newsAsync';

interface newsState {
  data: INews[];
  current: INews | undefined;
  loading: boolean;
  createErr: InpErrMap<INewsCreateData> | null;
}

const initialState: newsState = {
  data: [],
  current: undefined,
  loading: false,
  createErr: null,
};

const newsSlice = createSlice({
  name: 'news',
  initialState,
  reducers: {
    clearCurrent(state) {
      state.current = undefined;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(newsAdd.pending, (state) => {
      state.loading = true;
      state.createErr = null;
    });
    builder.addCase(newsAdd.rejected, (state, { payload }) => {
      state.loading = false;
      state.createErr = (payload as IErr<INewsCreateData>).errors;
    });
    builder.addCase(newsAdd.fulfilled, (state, { payload }) => {
      state.data.unshift(payload);
      state.loading = false;
    });
    builder.addCase(fetchNews.fulfilled, (state, { payload }) => {
      state.data = payload.reverse();
    });
    builder.addCase(fetchOneNews.fulfilled, (state, { payload }) => {
      state.current = payload;
    });
    builder.addCase(newsEdit.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(newsEdit.fulfilled, (state, { payload }) => {
      state.loading = false;
      state.current = payload;
    });
    builder.addCase(newsDelete.fulfilled, (state) => {
      state.current = undefined;
    });
  },
});

export const { clearCurrent } = newsSlice.actions;

export default newsSlice;
