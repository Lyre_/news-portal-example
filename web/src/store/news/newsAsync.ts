import { createAsyncThunk } from '@reduxjs/toolkit';
import $axios from '../../axios';
import { IErr, INews, INewsCreateData } from '../../types';
import { ADD_NEWS_URL, DELETE_NEWS_URL, EDIT_NEWS_URL, FETCH_NEWS_URL } from '../../constants';
import history from '../../components/NavigateSetter/History';
import { AxiosError } from 'axios';

export const fetchNews = createAsyncThunk('users/fetchNews', async (_, { rejectWithValue }) => {
  try {
    const { data } = await $axios.get<INews[]>(FETCH_NEWS_URL);
    return data;
  } catch (err) {
    const errors = err as AxiosError;
    if (!errors.response) {
      throw err;
    }
    return rejectWithValue(errors.response?.data);
  }
});

export const fetchOneNews = createAsyncThunk(
  'users/fetchOneNews',
  async (id: string, { rejectWithValue }) => {
    try {
      const { data } = await $axios.get<INews>(FETCH_NEWS_URL + id);
      return data;
    } catch (err) {
      const errors = err as AxiosError;
      if (!errors.response) {
        throw err;
      }
      return rejectWithValue(errors.response?.data);
    }
  }
);

export const newsAdd = createAsyncThunk(
  'users/newsAdd',
  async (input: INewsCreateData, { rejectWithValue }) => {
    try {
      const { data } = await $axios.post<INews>(ADD_NEWS_URL, input);
      history.push('/');
      return data;
    } catch (err) {
      const errors = err as AxiosError<IErr<INewsCreateData>>;
      if (!errors.response) {
        throw err;
      }
      return rejectWithValue(errors.response?.data);
    }
  }
);
export const newsEdit = createAsyncThunk(
  'users/newsEdit',
  async (input: INewsCreateData & { id: string }, { rejectWithValue }) => {
    try {
      const { data } = await $axios.put<INews>(EDIT_NEWS_URL, input);
      return data;
    } catch (err) {
      const errors = err as AxiosError<IErr<INewsCreateData>>;
      if (!errors.response) {
        throw err;
      }
      return rejectWithValue(errors.response?.data);
    }
  }
);
export const newsDelete = createAsyncThunk(
  'users/newsDelete',
  async (id: string, { rejectWithValue }) => {
    try {
      await $axios.delete<INews>(DELETE_NEWS_URL + id);
      history.push('/');
    } catch (err) {
      const errors = err as AxiosError<IErr<INewsCreateData>>;
      if (!errors.response) {
        throw err;
      }
      return rejectWithValue(errors.response?.data);
    }
  }
);
