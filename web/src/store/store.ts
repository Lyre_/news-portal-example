import { configureStore } from '@reduxjs/toolkit';
import userSlice from './user/userSlice';
import newsSlice from './news/newsSlice';

export const store = configureStore({
  reducer: {
    users: userSlice.reducer,
    news: newsSlice.reducer,
  },
});
