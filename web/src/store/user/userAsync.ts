import { createAsyncThunk } from '@reduxjs/toolkit';
import axios, { AxiosError } from 'axios';
import $axios from '../../axios';
import {
  USER_LOGOUT_URL,
  USER_REFRESH_TOKEN_URL,
  USER_SIGN_IN_URL,
  USER_SIGN_UP_URL,
} from '../../constants';
import { IErr, IUser, IUserRefreshed } from '../../types';
import history from '../../components/NavigateSetter/History';
import { API_URL } from '../../config';

export interface UserSignInData {
  email?: string;
  name?: string;
  password: string;
}

export interface UserSignUpData extends Omit<IUser, 'created_at' | '_id'> {
  password: string;
  confirmPassword: string;
}

export const clientLogin = createAsyncThunk(
  'users/clientLogin',
  async (input: UserSignInData, { rejectWithValue }) => {
    try {
      const { data } = await $axios.post<IUser>(USER_SIGN_IN_URL, input);
      history.push('/');
      return data;
    } catch (err) {
      const errors = err as AxiosError<IErr<UserSignInData>>;
      if (!errors.response) {
        throw err;
      }
      return rejectWithValue(errors.response?.data);
    }
  }
);

export const clientRegistration = createAsyncThunk(
  'users/clientRegistration',
  async (input: UserSignUpData, { rejectWithValue }) => {
    try {
      const { data } = await $axios.post<IUser>(USER_SIGN_UP_URL, input);
      history.push('/');
      return data;
    } catch (err) {
      const errors = err as AxiosError<IErr<UserSignUpData>>;
      if (!errors.response) {
        throw err;
      }
      return rejectWithValue(errors.response?.data);
    }
  }
);

export const clientLogout = createAsyncThunk(
  'users/clientLogout',
  async (_, { rejectWithValue }) => {
    try {
      await $axios.delete(USER_LOGOUT_URL);
      history.push('/');
    } catch (err) {
      const errors = err as AxiosError;
      if (!errors.response) {
        throw err;
      }
      return rejectWithValue(errors.response?.data);
    }
  }
);
export const checkAuth = createAsyncThunk('users/checkAuth', async (_, { rejectWithValue }) => {
  try {
    const { data } = await axios.get<IUserRefreshed>(API_URL + USER_REFRESH_TOKEN_URL, {
      withCredentials: true,
    });
    return data;
  } catch (err) {
    const errors = err as AxiosError;
    if (!errors.response) {
      throw err;
    }
    return rejectWithValue(errors.response?.data);
  }
});
