import { createSlice } from '@reduxjs/toolkit';
import { IErr, InpErrMap, IUser } from '../../types';
import {
  checkAuth,
  clientLogin,
  clientLogout,
  clientRegistration,
  UserSignInData,
  UserSignUpData,
} from './userAsync';

interface userState {
  client: IUser | null;
  loading: boolean;
  errors: InpErrMap<UserSignInData> | null;
  regErr: InpErrMap<UserSignUpData> | null;
}

const initialState: userState = {
  client: null,
  loading: false,
  errors: null,
  regErr: null,
};

const userSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(clientLogin.pending, (state) => {
      state.loading = true;
      state.errors = null;
    });
    builder.addCase(clientLogin.rejected, (state, { payload }) => {
      state.loading = false;
      state.errors = (payload as IErr<UserSignInData>).errors;
    });
    builder.addCase(clientLogin.fulfilled, (state, { payload }) => {
      state.client = payload;
      state.loading = false;
    });
    builder.addCase(clientRegistration.pending, (state) => {
      state.loading = true;
      state.regErr = null;
    });
    builder.addCase(clientRegistration.rejected, (state, { payload }) => {
      state.loading = false;
      state.regErr = (payload as IErr<UserSignUpData>).errors;
    });
    builder.addCase(clientRegistration.fulfilled, (state, { payload }) => {
      state.client = payload;
      state.loading = false;
    });
    builder.addCase(clientLogout.fulfilled, (state) => {
      localStorage.removeItem('access_token');
      state.client = null;
    });
    builder.addCase(checkAuth.fulfilled, (state, { payload }) => {
      state.client = payload.user;
    });
  },
});

export default userSlice;
