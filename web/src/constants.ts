//ROUTES PATHS
export const SIGN_IN_PATH = 'sign_in/';
export const SIGN_UP_PATH = 'sign_up/';
export const NEWS_PATH = 'news/';
export const ONE_NEWS_PATH = 'news/:id';
export const ADD_NEWS_PATH = 'news/add';

//API URLS
export const USER_SIGN_IN_URL = '/user/sign_in/';
export const USER_SIGN_UP_URL = '/user/sign_up/';
export const USER_REFRESH_TOKEN_URL = '/user/refresh/';
export const USER_LOGOUT_URL = '/user/logout/';
export const FETCH_NEWS_URL = '/news/';
export const ADD_NEWS_URL = '/news/add/';
export const EDIT_NEWS_URL = '/news/edit/';
export const DELETE_NEWS_URL = '/news/delete/';
