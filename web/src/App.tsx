import React from 'react';
import Layout from './components/Layout/Layout';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { ADD_NEWS_PATH, NEWS_PATH, ONE_NEWS_PATH, SIGN_IN_PATH, SIGN_UP_PATH } from './constants';
import SignIn from './components/SignIn/SignIn';
import SignUp from './components/SignUp/SignUp';
import News from './components/News/News';
import OneNews from './components/News/OneNews';
import NavigateSetter from './components/NavigateSetter/NavigateSetter';
import AddNews from './components/News/AddNews';

function App() {
  return (
    <BrowserRouter>
      <NavigateSetter />
      <Layout>
        <Routes>
          <Route path="/" element={<News />} />
          <Route path={SIGN_IN_PATH} element={<SignIn />} />
          <Route path={SIGN_UP_PATH} element={<SignUp />} />
          <Route path={NEWS_PATH} element={<News />} />
          <Route path={ONE_NEWS_PATH} element={<OneNews />} />
          <Route path={ADD_NEWS_PATH} element={<AddNews />} />
          <Route path={'*'} element={<div style={{ marginTop: '20px' }}>not implemented</div>} />
        </Routes>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
